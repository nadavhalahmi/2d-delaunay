#pragma once
#include <vector>
#include "Triangle.hpp"

using namespace std;

class Delaunay
{
private:
    vector<Triangle *> triangles;
    array<Point *, 3> dummy_points;
    Triangle* last_visited_triangle;

public:
    Delaunay();
    Delaunay(vector<Point *> points);
    array<Point *, 3> getDummyPoints();
    Triangle *get_containing_triangle(const Point *p);
    void insert(const Point *p);
    vector<Triangle *> &getTriangles();
    void flip(Triangle *t1, const Point *p, pair<Triangle *, const Point *> neighbor);
    void print(ostream &os) const;
    vector<Triangle *> getPointTriangles(const Point *p);
    template<typename Pred>
    Triangle *walk(const Point *p, Pred& pred, Triangle* start_triangle = nullptr);
    void addPointsForVoronoi(array<Point, 2> boundries);
    bool test_delaunay_neighbors_symetric();
    bool test_delaunay_criterion();
    bool test_delaunay_triangles_are_legal();
};
ostream &operator<<(ostream &os, const Delaunay &delaunay);