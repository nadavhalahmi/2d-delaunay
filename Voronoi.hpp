#pragma once
#include <vector>
#include "Polygon.hpp"
#include "Delaunay.hpp"

using namespace std;

class Voronoi
{
private:
    vector<Polygon *> polygons;

public:
    Voronoi(Delaunay &delaunay, array<Point, 2> boundries);
    void print(ostream &os) const;
};
ostream &operator<<(ostream &os, const Voronoi &voronoi);