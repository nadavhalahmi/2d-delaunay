#include <iostream>
#include "Point.hpp"
#include "Triangle.hpp"
#include <assert.h>
#include "Delaunay.hpp"
#include "Voronoi.hpp"
#include <fenv.h>

// TODO: MOVE this function to other file
double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}


bool test_two_triangles_are_equal()
{
    Point p1(0.0, 0.0);
    Point p2(0.0, 1.0);
    Point p3(1.0, 0.0);

    Triangle t1(&p1, &p2, &p3);
    Triangle t2(&p2, &p1, &p3);

    return t1 == t2;
}

bool test_delaunay_creation()
{
    Delaunay delaunay;
    return delaunay.getTriangles().size() == 1;
}

bool test_delaunay_insert_first_point()
{
    Delaunay delaunay;
    delaunay.insert(new Point(0, 0));
    return delaunay.getTriangles().size() == 3;
}

bool test_set_get_neighbor()
{
    Delaunay delaunay;
    delaunay.insert(new Point(0, 0));
    return delaunay.getTriangles().size() == 3;
}

bool test_delaunay_insert_many_points()
{
    Delaunay delaunay;
    const Point p1(-10, -10), p2(10, 10); // TODO: assert p1 and p2 are inside dummy triangle
    const int NUM_POINTS = 1000;
    for (int i = 0; i < NUM_POINTS; i++)
    {
        Point *p = new Point(fRand(p1.getX()+1, p2.getX()-1), fRand(p1.getY()+1, p2.getY()-1));
        delaunay.insert(p);
        assert(delaunay.test_delaunay_neighbors_symetric());
        assert(delaunay.test_delaunay_criterion());
        assert(delaunay.test_delaunay_triangles_are_legal());
    }
    // std::cout << delaunay;
    return true;
}

bool test_delaunay_insert_many_points_through_points_constructor()
{
    vector<Point *> points;
    const Point p1(-10, -10), p2(10, 10); // TODO: assert p1 and p2 are inside dummy triangle
    const int NUM_POINTS = 1000;
    for (int i = 0; i < NUM_POINTS; i++)
    {
        points.push_back(new Point(fRand(p1.getX()+1, p2.getX()-1), fRand(p1.getY()+1, p2.getY()-1)));
    }
    Delaunay delaunay(points);
    //std::cout << delaunay;
    return true;
}

bool test_delaunay_cartesian_grid()
{
    Delaunay delaunay;
    assert(delaunay.test_delaunay_neighbors_symetric());
    assert(delaunay.test_delaunay_criterion());
    //std::cout << delaunay;
    const Point p1(-10, -10), p2(10, 10); // TODO: assert p1 and p2 are inside dummy triangle
    for (int x = p1.getX()+1; x <= p2.getX()-1; x++)
    {
        for (int y = p1.getY()+1; y <= p2.getY()-1; y++)
        {
            Point *p = new Point(x, y);
            delaunay.insert(p);
            assert(delaunay.test_delaunay_neighbors_symetric());
            assert(delaunay.test_delaunay_criterion());
            assert(delaunay.test_delaunay_triangles_are_legal());
        }
    }
    //std::cout << delaunay;
    return true;
}

//TODO: FIX code duplication below
bool test_voronoi_from_delaunay()
{
    Delaunay delaunay;
    array<Point, 2> boundries = {Point(-10, -10), Point(10, 10)};
    Point p1 = boundries[0];
    Point p2 = boundries[1];
    const int NUM_POINTS = 1000;
    for (int i = 0; i < NUM_POINTS; i++)
    {
        // if(i % 1000 == 0) cout << i <<endl;
        Point *p = new Point(fRand(p1.getX()+1, p2.getX()-1), fRand(p1.getY()+1, p2.getY()-1));
        delaunay.insert(p);
        assert(delaunay.test_delaunay_neighbors_symetric());
        assert(delaunay.test_delaunay_criterion());
        assert(delaunay.test_delaunay_triangles_are_legal());
    }
    // std::cout << delaunay;
    Voronoi voronoi(delaunay, boundries);
    //std:: cout << voronoi;
    return true;
}

bool test_voronoi_from_delaunay_through_points_constructor()
{
    array<Point, 2> boundries = {Point(-10, -10), Point(10, 10)};
    vector<Point *> points;
    const Point p1(-10, -10), p2(10, 10); // TODO: assert p1 and p2 are inside dummy triangle
    const int NUM_POINTS = 1000000;
    for (int i = 0; i < NUM_POINTS; i++)
    {
        points.push_back(new Point(fRand(p1.getX()+1, p2.getX()-1), fRand(p1.getY()+1, p2.getY()-1)));
    }
    Delaunay delaunay(points);
    // std::cout << delaunay;
    Voronoi voronoi(delaunay, boundries);
    //std:: cout << voronoi;
    return true;
}

//TODO: FIX code duplication below
bool test_voronoi_from_delaunay_caretesian_grid()
{
    Delaunay delaunay;
    assert(delaunay.test_delaunay_neighbors_symetric());
    assert(delaunay.test_delaunay_criterion());
    //std::cout << delaunay;
    array<Point, 2> boundries = {Point(-10, -10), Point(10, 10)};
    Point p1 = boundries[0];
    Point p2 = boundries[1];
    for (int x = p1.getX()+1; x <= p2.getX()-1; x++)
    {
        for (int y = p1.getY()+1; y <= p2.getY()-1; y++)
        {
            Point *p = new Point(x, y);
            delaunay.insert(p);
            assert(delaunay.test_delaunay_neighbors_symetric());
            assert(delaunay.test_delaunay_criterion());
            assert(delaunay.test_delaunay_triangles_are_legal());
        }
    }
    // std::cout << delaunay;
    Voronoi voronoi(delaunay, boundries);
    // std:: cout << voronoi;
    return true;
}

bool test_voronoi_from_delaunay_caretesian_grid_points_constructor()
{
    array<Point, 2> boundries = {Point(-10, -10), Point(10, 10)};
    vector<Point *> points;
    Point p1 = boundries[0];
    Point p2 = boundries[1];
    for (int x = p1.getX()+1; x <= p2.getX()-1; x++)
    {
        for (int y = p1.getY()+1; y <= p2.getY()-1; y++)
        {
            Point *p = new Point(x, y);
            points.push_back(p);
        }
    }
    Delaunay delaunay(points);
    assert(delaunay.test_delaunay_neighbors_symetric());
    assert(delaunay.test_delaunay_criterion());
    assert(delaunay.test_delaunay_triangles_are_legal());
    // std::cout << delaunay;
    Voronoi voronoi(delaunay, boundries);
    std:: cout << voronoi;
    return true;
}

int main()
{
    feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);  // Enable all floating point exceptions but FE_INEXACT
    //test_two_triangles_are_equal();
    //test_delaunay_creation();
    //test_delaunay_insert_first_point();
    //test_delaunay_insert_many_points();
    //test_delaunay_cartesian_grid();
    //test_delaunay_insert_many_points_through_points_constructor();
    //test_voronoi_from_delaunay();
    //test_voronoi_from_delaunay_caretesian_grid();
    test_voronoi_from_delaunay_through_points_constructor();
    //test_voronoi_from_delaunay_caretesian_grid_points_constructor();
    return 0;
}
