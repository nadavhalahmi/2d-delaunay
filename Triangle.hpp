#pragma once
#include <array>
#include "Point.hpp"

#define REAL double /* float or double */

extern "C" REAL orient2d(REAL *pa, REAL *pb, REAL *pc);
extern "C" REAL incircle(REAL *pa, REAL *pb, REAL *pc, REAL *pd);

using namespace std;

class Triangle
{
private:
    array<const Point *, 3> points;
    array<pair<Triangle *, const Point *>, 3> neighbors;
    int idx;

public:
    Triangle(const Point *p1, const Point *p2, const Point *p3, int idx=-1);
    bool operator==(Triangle t2);
    static double order(const array<const Point *, 3> &points);
    double order();
    const Point *operator[](int i);
    bool contains(const Point *p);
    pair<const Point *, const Point *> getOtherTwoPoints(const Point *p) const;
    int getPointIndex(const Point *p) const;
    int getIdx() const;
    pair<Triangle *, const Point *> getNeighbor(const Point *p) const;
    pair<Triangle *, const Point *> getNeighbor(const int &i) const;
    void setNeighbors(const array<pair<Triangle *, const Point *>, 3> &neighbors);
    void setNeighbor(const Point *point, pair<Triangle *, const Point *> neighbor);
    bool checkDelaunayCriterion(const Point *point, const pair<Triangle *, const Point *> &neighbor) const;
    void setPoints(array<const Point *, 3> &points);
    void print(ostream &os) const;
    array<const Point *, 3> &getPoints();
    Point* getCenterPoint();
    double getCircleRadius();
};
ostream &operator<<(ostream &os, const Triangle &triangle);