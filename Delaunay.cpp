#include "Delaunay.hpp"
#include <algorithm>
#include <assert.h>
#include <set>
#include <unordered_set>
#include <map>
#include <stdexcept>
#include <limits>
#include "utils.hpp"

extern "C" void exactinit();

/*
 * creates a new Delaunay instance, including an initial dummy Triangle
 */
Delaunay::Delaunay() : dummy_points({new Point(0, 100), new Point(-100, -100), new Point(100, -100)}), last_visited_triangle(nullptr)
{
    exactinit();
    this->triangles.push_back(new Triangle(dummy_points[0], dummy_points[1], dummy_points[2], 0));
    this->last_visited_triangle = this->triangles[0];
}

Delaunay::Delaunay(vector<Point *> points)
{
    exactinit();
    double min_x = (*std::min_element(
                        points.begin(), points.end(),
                        [](const auto &a, const auto &b)
                        { return a->getX() < b->getX(); }))
                       ->getX();
    double min_y = (*std::min_element(
                        points.begin(), points.end(),
                        [](const auto &a, const auto &b)
                        { return a->getY() < b->getY(); }))
                       ->getY();
    double max_x = (*std::max_element(
                        points.begin(), points.end(),
                        [](const auto &a, const auto &b)
                        { return a->getX() < b->getX(); }))
                       ->getX();
    double max_y = (*std::max_element(
                        points.begin(), points.end(),
                        [](const auto &a, const auto &b)
                        { return a->getY() < b->getY(); }))
                       ->getY();
    sort(begin(points), end(points), [=](Point* p1, Point* p2)
    {
        int n = 1024; // TODO: CHECK this is correct
        int x1 = ((p1->getX() - min_x) / (max_x - min_x)) * n;
        int y1 = ((p1->getY() - min_y) / (max_y - min_y)) * n;
        int x2 = ((p2->getX() - min_x) / (max_x - min_x)) * n;
        int y2 = ((p2->getY() - min_y) / (max_y - min_y)) * n;
        return xy2d(n, x1, y1) < xy2d(n, x2, y2);
    });
    dummy_points = {new Point(0, 100), new Point(-100, -100), new Point(100, -100)}; // TODO: CHANGE based on mins and maxes
    this->triangles.push_back(new Triangle(dummy_points[0], dummy_points[1], dummy_points[2], 0));
    this->last_visited_triangle = this->triangles[0];
    for (int i = 0; i < points.size(); i++)
    {
        this->insert(points[i]);
        assert(this->test_delaunay_neighbors_symetric());
        assert(this->test_delaunay_criterion());
        assert(this->test_delaunay_triangles_are_legal());
    }
}

array<Point *, 3> Delaunay::getDummyPoints()
{
    return this->dummy_points;
}

/*
 * inserts the point p into the delaunay instance by creating 3 new triangles (and deleting the one of p)
 * and making flips as necessary
 */
void Delaunay::insert(const Point *p)
{
    auto p_inside_triangle = [](Triangle *t, const Point *p)
    {
        return t->contains(p);
    };
    Triangle *triangle_of_p = this->walk(p, p_inside_triangle, last_visited_triangle);
    assert(triangle_of_p->contains(p));
    //~~~~~~~~~~~~~~~~~~~~
    Triangle *t0 = new Triangle((*triangle_of_p)[0], (*triangle_of_p)[1], p, triangle_of_p->getIdx());
    Triangle *t1 = new Triangle(p, (*triangle_of_p)[1], (*triangle_of_p)[2], this->triangles.size());
    Triangle *t2 = new Triangle((*triangle_of_p)[0], p, (*triangle_of_p)[2], this->triangles.size()+1);
    t0->setNeighbors({pair<Triangle *, const Point *>(t1, (*triangle_of_p)[2]),
                      pair<Triangle *, const Point *>(t2, (*triangle_of_p)[2]),
                      pair<Triangle *, const Point *>(triangle_of_p->getNeighbor((*triangle_of_p)[2]))});
    t1->setNeighbors({pair<Triangle *, const Point *>(triangle_of_p->getNeighbor((*triangle_of_p)[0])),
                      pair<Triangle *, const Point *>(t2, (*triangle_of_p)[0]),
                      pair<Triangle *, const Point *>(t0, (*triangle_of_p)[0])});
    t2->setNeighbors({pair<Triangle *, const Point *>(t1, (*triangle_of_p)[1]),
                      pair<Triangle *, const Point *>(triangle_of_p->getNeighbor((*triangle_of_p)[1])),
                      pair<Triangle *, const Point *>(t0, (*triangle_of_p)[1])});
    pair<Triangle *, const Point *> neighbor = triangle_of_p->getNeighbor(0);
    if (neighbor.first)
        neighbor.first->setNeighbor(neighbor.second, pair<Triangle *, const Point *>(t1, p));
    neighbor = triangle_of_p->getNeighbor(1);
    if (neighbor.first)
        neighbor.first->setNeighbor(neighbor.second, pair<Triangle *, const Point *>(t2, p));
    neighbor = triangle_of_p->getNeighbor(2);
    if (neighbor.first)
        neighbor.first->setNeighbor(neighbor.second, pair<Triangle *, const Point *>(t0, p));
    // delete triangle_of_p; // TODO: think of how to delete without destroying anything
    triangles[triangle_of_p->getIdx()] = t0;
    triangles.push_back(t1);
    triangles.push_back(t2);
    //~~~~~~~~~~~~~~~~~~~~~
    vector<Triangle *> triangles_to_check;
    triangles_to_check.push_back(t0);
    triangles_to_check.push_back(t1);
    triangles_to_check.push_back(t2);
    while (triangles_to_check.size() > 0)
    {
        Triangle *t = triangles_to_check.back();
        triangles_to_check.pop_back();
        pair<Triangle *, const Point *> neighbor = t->getNeighbor(p);
        if (neighbor.first)
        {
            if (!t->checkDelaunayCriterion(p, neighbor))
            {
                this->flip(t, p, neighbor);
                triangles_to_check.push_back(t);
                triangles_to_check.push_back(neighbor.first);
            }
        }
    }
    last_visited_triangle = t0;
}

/*
 * returns the triangles vector, used for testing
 */
vector<Triangle *> &Delaunay::getTriangles()
{
    return this->triangles;
}

/*
 * flips t1 this neighbor
 * the non-common points of these triangles are p and the point in neighbor
 */
void Delaunay::flip(Triangle *t1, const Point *p, pair<Triangle *, const Point *> neighbor)
{
    pair<const Point *, const Point *> other_points = t1->getOtherTwoPoints(p);
    assert(neighbor.first->getPointIndex(other_points.first) >= 0);
    assert(neighbor.first->getPointIndex(other_points.second) >= 0);
    // the next calls to setPoints will keep p in the same location as it was before
    array<const Point *, 3> points = {p, neighbor.second, other_points.first};
    array<pair<Triangle *, const Point *>, 3> t1_old_neighbors_array = {t1->getNeighbor(other_points.first),
                                                                        t1->getNeighbor(other_points.second),
                                                                        t1->getNeighbor(p)};
    array<pair<Triangle *, const Point *>, 3> neighbor_old_neighbors_array = {neighbor.first->getNeighbor(other_points.first),
                                                                              neighbor.first->getNeighbor(other_points.second),
                                                                              neighbor.first->getNeighbor(neighbor.second)};
    t1->setPoints(points);
    array<pair<Triangle *, const Point *>, 3> neighbors_array;
    neighbors_array[t1->getPointIndex(p)] = neighbor_old_neighbors_array[1];
    neighbors_array[t1->getPointIndex(neighbor.second)] = t1_old_neighbors_array[1];
    neighbors_array[t1->getPointIndex(other_points.first)] = pair<Triangle *, const Point *>(neighbor.first, other_points.second);
    t1->setNeighbors(neighbors_array);
    if (t1_old_neighbors_array[1].first)
        t1_old_neighbors_array[1].first->setNeighbor(t1_old_neighbors_array[1].second, pair<Triangle *, const Point *>(t1, neighbor.second));
    if (neighbor_old_neighbors_array[1].first)
        neighbor_old_neighbors_array[1].first->setNeighbor(neighbor_old_neighbors_array[1].second, pair<Triangle *, const Point *>(t1, p));

    array<const Point *, 3> points2 = {p, neighbor.second, other_points.second};
    neighbor.first->setPoints(points2);
    neighbors_array[neighbor.first->getPointIndex(p)] = neighbor_old_neighbors_array[0];
    neighbors_array[neighbor.first->getPointIndex(neighbor.second)] = t1_old_neighbors_array[0];
    neighbors_array[neighbor.first->getPointIndex(other_points.second)] = pair<Triangle *, const Point *>(t1, other_points.first);
    neighbor.first->setNeighbors(neighbors_array);
    if (t1_old_neighbors_array[0].first)
        t1_old_neighbors_array[0].first->setNeighbor(t1_old_neighbors_array[0].second, pair<Triangle *, const Point *>(neighbor.first, neighbor.second));
    if (neighbor_old_neighbors_array[0].first)
        neighbor_old_neighbors_array[0].first->setNeighbor(neighbor_old_neighbors_array[0].second, pair<Triangle *, const Point *>(neighbor.first, p));
}

template <typename Pred>
Triangle *Delaunay::walk(const Point *p, Pred &pred, Triangle *start_triangle)
{
    Triangle *t = start_triangle;
    assert(t != nullptr);
    int i = 0; // just to make sure we are not in an infinite loop
    while (i < this->triangles.size())
    {
        if (pred(t, p))
            return t;
        double p0[2] = {(*t)[0]->getX(), (*t)[0]->getY()};
        double p1[2] = {(*t)[1]->getX(), (*t)[1]->getY()};
        double p2[2] = {(*t)[2]->getX(), (*t)[2]->getY()};
        double p3[2] = {p->getX(), p->getY()};
        if (orient2d(p0, p1, p3) < 0 - std::numeric_limits<double>::min())
        {
            t = t->getNeighbor(2).first;
        }
        else if (orient2d(p1, p2, p3) < 0 - std::numeric_limits<double>::min())
        {
            t = t->getNeighbor(0).first;
        }
        else if (orient2d(p2, p0, p3) < 0 - std::numeric_limits<double>::min())
        {
            t = t->getNeighbor(1).first;
        }
        else
        {
            throw exception();
        }
        i++;
    }
    throw exception(); // TODO: ADD message to print values of p
}

vector<Triangle *> Delaunay::getPointTriangles(const Point *p)
{
    vector<Triangle *> point_triangles;
    // find first triangle of p
    auto p_is_part_of_triangle = [](Triangle *t, const Point *p)
    {
        return any_of(begin(t->getPoints()), end(t->getPoints()), [=](const Point *point)
                      { return point == p; });
    };
    Triangle *triangle = this->walk(p, p_is_part_of_triangle, last_visited_triangle);
    last_visited_triangle = triangle;
    point_triangles.push_back(triangle);
    // go over neighbors until we complete a loop
    auto next_triangle = (triangle)->getNeighbor(((triangle)->getPointIndex(p) + 1) % 3);
    while (next_triangle.first != triangle)
    {
        point_triangles.push_back(next_triangle.first);
        next_triangle = next_triangle.first->getNeighbor((next_triangle.first->getPointIndex(p) + 1) % 3);
    }

    return point_triangles;
}

void Delaunay::addPointsForVoronoi(array<Point, 2> boundries)
{
    map<const Point *, array<bool, 4>> created_points;
    vector<const Point *> points_to_add;
    unordered_set<const Point *> visited_points;
    // add dummy points to visited_points so we dont create voronoi cells for these points
    for (auto dummy_point : dummy_points)
    {
        visited_points.insert(dummy_point);
    }
    for (auto triangle : triangles)
    {
        for (auto point : triangle->getPoints())
        {
            if (visited_points.find(point) != visited_points.end()) // already have this point
                continue;
            visited_points.insert(point);
            created_points[point] = {false};
            for (auto triangle : this->getPointTriangles(point))
            {
                double radius = triangle->getCircleRadius();
                Point *center = triangle->getCenterPoint();
                Point *other_point;
                if (!created_points[point][0] && center->getX() - radius < boundries[0].getX())
                {
                    other_point = new Point(2 * boundries[0].getX() - point->getX(), point->getY());
                    points_to_add.push_back(other_point);
                    created_points[point][0] = true;
                }
                if (!created_points[point][1] && center->getX() + radius > boundries[1].getX())
                {
                    other_point = new Point(2 * boundries[1].getX() - point->getX(), point->getY());
                    points_to_add.push_back(other_point);
                    created_points[point][1] = true;
                }
                if (!created_points[point][2] && center->getY() - radius < boundries[0].getY())
                {
                    other_point = new Point(point->getX(), 2 * boundries[0].getY() - point->getY());
                    points_to_add.push_back(other_point);
                    created_points[point][2] = true;
                }
                if (!created_points[point][3] && center->getY() + radius > boundries[1].getY())
                {
                    other_point = new Point(point->getX(), 2 * boundries[1].getY() - point->getY());
                    points_to_add.push_back(other_point);
                    created_points[point][3] = true;
                }
            }
        }
    }
    for (auto point : points_to_add)
    {
        this->insert(point);
        assert(this->test_delaunay_neighbors_symetric());
        assert(this->test_delaunay_criterion());
        assert(this->test_delaunay_triangles_are_legal());
    }
}

bool Delaunay::test_delaunay_neighbors_symetric()
{
    for (auto triangle : this->getTriangles())
    {
        pair<Triangle *, const Point *> neighbor = triangle->getNeighbor(0);
        if (neighbor.first)
            assert(neighbor.first->getNeighbor(neighbor.second) == (pair<Triangle *, const Point *>(triangle, (*triangle)[0])));
        neighbor = triangle->getNeighbor(1);
        if (neighbor.first)
            assert(neighbor.first->getNeighbor(neighbor.second) == (pair<Triangle *, const Point *>(triangle, (*triangle)[1])));
        neighbor = triangle->getNeighbor(2);
        if (neighbor.first)
            assert(neighbor.first->getNeighbor(neighbor.second) == (pair<Triangle *, const Point *>(triangle, (*triangle)[2])));
    }
    return true;
}

bool Delaunay::test_delaunay_triangles_are_legal()
{
    for (auto triangle : this->getTriangles())
    {
        double area = triangle->order(); // TOD: rename order function
        if (area <= std::numeric_limits<double>::min() && area >= -std::numeric_limits<double>::min())
        {
            return false;
        }
    }
    return true;
}

bool Delaunay::test_delaunay_criterion()
{
    for (auto triangle : this->getTriangles())
    {
        for (int i = 0; i < 3; i++)
            assert(triangle->checkDelaunayCriterion((*triangle)[i], triangle->getNeighbor(i)));
    }
    return true;
}

void Delaunay::print(ostream &os) const
{
    for (auto triangle : this->triangles)
    {
        os << *triangle;
    }
    os << endl;
}

ostream &operator<<(ostream &os, const Delaunay &delaunay)
{
    delaunay.print(os); // assuming you define print for matrix
    return os;
}