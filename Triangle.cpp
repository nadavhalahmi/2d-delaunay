#include "Triangle.hpp"
#include <assert.h>
#include <limits>
#include <cmath>


/*
 * creates a new triangle with no neighbors with Triangle::order() > 0
 */
Triangle::Triangle(const Point *p1, const Point *p2, const Point *p3, int idx)
    : points({p1, p2, p3}), neighbors({make_pair(nullptr, nullptr), make_pair(nullptr, nullptr), make_pair(nullptr, nullptr)}), idx(idx)
{
    if (this->order(this->points) < 0)
    {
        swap(this->points[0], this->points[1]);
    }
    assert(this->order(this->points) > 0 - std::numeric_limits<double>::min());
}

/*
 * returns if this triangle is equalt to t2.
 * two triangles are considered equal if they consist of the same points (by value)
 */
bool Triangle::operator==(Triangle t2)
{
    return this->points == t2.points;
}

/*
 * returns orient2d of the points based on their order
 */
double Triangle::order(const array<const Point *, 3> &points)
{
    double p0[2] = {points[0]->getX(), points[0]->getY()};
    double p1[2] = {points[1]->getX(), points[1]->getY()};
    double p2[2] = {points[2]->getX(), points[2]->getY()};
    double retval = orient2d(p0, p1, p2);
    // if(nearly_equal(retval, 0)) throw exception(); //TODO: ADD MESSAGE
    return retval;
}

/*
 * returns orient2d of the points based on their order
 */
double Triangle::order()
{
    double p0[2] = {points[0]->getX(), points[0]->getY()};
    double p1[2] = {points[1]->getX(), points[1]->getY()};
    double p2[2] = {points[2]->getX(), points[2]->getY()};
    double retval = orient2d(p0, p1, p2);
    // if(nearly_equal(retval, 0)) throw exception(); //TODO: ADD MESSAGE
    return retval;
}

/*
 * returns one of three points based on an index (0-2)
 */
const Point *Triangle::operator[](int i)
{
    return this->points[i];
}

/*
 * returns wether this triangle contains p
 */
bool Triangle::contains(const Point *p)
{
    double p0[2] = {this->points[0]->getX(), this->points[0]->getY()};
    double p1[2] = {this->points[1]->getX(), this->points[1]->getY()};
    double p2[2] = {this->points[2]->getX(), this->points[2]->getY()};
    double p3[2] = {p->getX(), p->getY()};
    return ((orient2d(p0, p1, p3) > 0 - std::numeric_limits<double>::min()) && (orient2d(p1, p2, p3) > 0 - std::numeric_limits<double>::min()) && (orient2d(p2, p0, p3) > 0 - std::numeric_limits<double>::min())) ||
           ((orient2d(p0, p1, p3) < 0 + std::numeric_limits<double>::min()) && (orient2d(p1, p2, p3) < 0 + std::numeric_limits<double>::min()) && (orient2d(p2, p0, p3) < 0 + std::numeric_limits<double>::min()));
}

/*
 * returns the other two points (which are not p) of this triangle
 */
pair<const Point *, const Point *> Triangle::getOtherTwoPoints(const Point *p) const
{
    if (this->points[0] == p)
        return pair<const Point *, const Point *>(this->points[1], this->points[2]);
    if (this->points[1] == p)
        return pair<const Point *, const Point *>(this->points[0], this->points[2]);
    if (this->points[2] == p)
        return pair<const Point *, const Point *>(this->points[0], this->points[1]);
    throw exception(); // TODO: add message of p doesnt exist in triangle
}

/*
 * returns the index of point p in this triangle (0-2)
 */
int Triangle::getPointIndex(const Point *p) const
{
    for (int i = 0; i < this->points.size(); i++)
    {
        if (this->points[i] == p)
            return i;
    }
    return -1;
}

int Triangle::getIdx() const
{
    return idx;
}

/*
 * returns the neighbor of this triangle related to p
 */
pair<Triangle *, const Point *> Triangle::getNeighbor(const Point *p) const
{
    int point_index = this->getPointIndex(p);
    return this->getNeighbor(point_index);
}

/*
 * returns the i's neighbor in neighbors array of this
 */
pair<Triangle *, const Point *> Triangle::getNeighbor(const int &i) const
{
    if (i < 0)
        throw exception(); // TODO: ADD message
    assert(!this->neighbors[i].first || this->neighbors[i].first->getPointIndex(this->neighbors[i].second) >= 0);
    return this->neighbors[i];
}

/*
 * updates this->neighbors vector to have the values of recieved neighbors vector
 */
void Triangle::setNeighbors(const array<pair<Triangle *, const Point *>, 3> &neighbors)
{
    for (int i = 0; i < this->neighbors.size(); i++)
    {
        this->neighbors[i] = neighbors[i];
    }
}

void Triangle::setNeighbor(const Point *point, pair<Triangle *, const Point *> neighbor)
{
    int point_index = this->getPointIndex(point);
    if (point_index < 0)
        throw exception(); // TODO: ADD message
    this->neighbors[point_index] = neighbor;
}

/*
 * returns true iff point is not inside neighbor and neihbor's point is no inside this
 */
bool Triangle::checkDelaunayCriterion(const Point *point, const pair<Triangle *, const Point *> &neighbor) const
{
    if (!neighbor.first)
        return true;
    double p0[2] = {this->points[0]->getX(), this->points[0]->getY()};
    double p1[2] = {this->points[1]->getX(), this->points[1]->getY()};
    double p2[2] = {this->points[2]->getX(), this->points[2]->getY()};
    double p3[2] = {neighbor.second->getX(), neighbor.second->getY()};
    if (incircle(p0, p1, p2, p3) > (0 + std::numeric_limits<double>::min()))
    {
        return false;
    }
    double q0[2] = {neighbor.first->points[0]->getX(), neighbor.first->points[0]->getY()};
    double q1[2] = {neighbor.first->points[1]->getX(), neighbor.first->points[1]->getY()};
    double q2[2] = {neighbor.first->points[2]->getX(), neighbor.first->points[2]->getY()};
    double q3[2] = {point->getX(), point->getY()};
    if (incircle(q0, q1, q2, q3) > (0 + std::numeric_limits<double>::min()))
    {
        return false;
    }
    return true;
}

/*
 * updates this->points vector to have recieved points vector values.
 * rearrange the this->points vector if necessary so that this->order() > 0
 * if points[0] is in this->points, keep it in the same location
 */
void Triangle::setPoints(array<const Point *, 3> &points)
{
    int p_index = this->getPointIndex(points[0]);
    if (p_index != 0 && p_index != -1)
    {
        swap(points[0], points[p_index]);
    }
    // now points[p_index] == original points[0]
    if (Triangle::order(points) < 0)
    {
        swap(points[(p_index + 1) % 3], points[(p_index + 2) % 3]);
    }
    for (int i = 0; i < this->points.size(); i++)
    {
        this->points[i] = points[i];
    }
    assert(this->order(this->points) > 0);
}

void Triangle::print(ostream &os) const
{
    os << "[";
    for (auto point : this->points)
    {
        os << *point;
    }
    os << "]";
}

array<const Point *, 3> &Triangle::getPoints()
{
    return points;
}

Point *Triangle::getCenterPoint()
{
    double x1 = points[0]->getX();
    double x2 = points[1]->getX();
    double x3 = points[2]->getX();
    double y1 = points[0]->getY();
    double y2 = points[1]->getY();
    double y3 = points[2]->getY();
    // Do we have a case where two point are very close compared to the third?
    const double d12 = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    const double d23 = (x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2);
    const double d13 = (x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3);
    if (d23 < 0.1 * (d13 + d12))
    {
        x1 -= x2;
        x3 -= x2;
        y1 -= y2;
        y3 -= y2;
        const double d_inv = 1 / (2 * (x3 * y1 - y3 * x1));
        return new Point((y1 * (x3 * x3 + y3 * y3) - y3 * (x1 * x1 + y1 * y1)) * d_inv + x2,
                         (x3 * (x1 * x1 + y1 * y1) - x1 * (x3 * x3 + y3 * y3)) * d_inv + y2);
    }
    x2 -= x1;
    x3 -= x1;
    y2 -= y1;
    y3 -= y1;
    const double d_inv = 1 / (2 * (x2 * y3 - y2 * x3));
    return new Point((y3 * (x2 * x2 + y2 * y2) - y2 * (x3 * x3 + y3 * y3)) * d_inv + x1,
                     (-x3 * (x2 * x2 + y2 * y2) + x2 * (x3 * x3 + y3 * y3)) * d_inv + y1);
}

double Triangle::getCircleRadius()
{
    const double big = 1e10;
    std::array<double, 3> sides;
    for (int i = 0; i < 3; ++i)
        sides[i] = points[i]->distance(points[(i + 1) % 3]);
    std::array<double, 3> temps;
    for (size_t i = 0; i < 3; ++i)
    {
        temps[i] = sides[(i + 1) % 3] + sides[(i + 2) % 3] - sides[i];
        if (temps[i] <= 0)
            return (sides[i] > big * sides[(i + 1) % 3] ||
                    sides[i] > big * sides[(i + 2) % 3])
                       ? 0.5 * sides[i]
                       : 0.5 * (sides[(i + 1) % 3] + sides[(i + 2) % 3]);
    }
    return sides[0] * sides[1] * sides[2] /
           sqrt((sides[0] + sides[1] + sides[2]) *
                temps[0] * temps[1] * temps[2]);
}

ostream &operator<<(ostream &os, const Triangle &triangle)
{
    triangle.print(os); // assuming you define print for matrix
    return os;
}