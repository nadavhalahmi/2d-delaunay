#include "Polygon.hpp"
#include <algorithm>
#include <assert.h>

void Polygon::insert(Point* p)
{
    this->points.push_back(p);
}

void Polygon::print(ostream &os) const
{
    os << "[";
    for (auto point : this->points)
    {
        os << *point;
    }
    os << "]";
}

ostream &operator<<(ostream &os, const Polygon &polygon)
{
    polygon.print(os); // assuming you define print for matrix
    return os;
}