#include "Voronoi.hpp"
#include <algorithm>
#include <set>
#include <assert.h>

Voronoi::Voronoi(Delaunay &delaunay, array<Point, 2> boundries)
{
    assert(delaunay.test_delaunay_neighbors_symetric());
    assert(delaunay.test_delaunay_criterion());
    assert(delaunay.test_delaunay_triangles_are_legal());
    delaunay.addPointsForVoronoi(boundries);
    assert(delaunay.test_delaunay_neighbors_symetric());
    assert(delaunay.test_delaunay_criterion());
    assert(delaunay.test_delaunay_triangles_are_legal());
    set<const Point *> visited_points;
    // add dummy points to visited_points so we dont create voronoi cells for these points
    for(auto dummy_point : delaunay.getDummyPoints())
    {
        visited_points.insert(dummy_point);
    }
    for (auto triangle : delaunay.getTriangles())
    {
        for (auto point : triangle->getPoints())
        {
            if (visited_points.count(point))
                continue;
            if(point->getX() < boundries[0].getX() || point->getX() > boundries[1].getX() || point->getY() < boundries[0].getY() || point->getY() > boundries[1].getY())
                continue;
            auto point_polygon = new Polygon();
            visited_points.insert(point);
            for (auto triangle : delaunay.getPointTriangles(point))
            {
                point_polygon->insert(triangle->getCenterPoint());
            }
            this->polygons.push_back(point_polygon);
        }
    }
}

void Voronoi::print(ostream &os) const
{
    for (auto polygon : this->polygons)
    {
        os << *polygon;
    }
    os << endl;
}

ostream &operator<<(ostream &os, const Voronoi &voronoi)
{
    voronoi.print(os); // assuming you define print for matrix
    return os;
}