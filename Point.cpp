#include "Point.hpp"
#include <cmath>

/*
 * creates the point (x,y)
 */
Point::Point(const double &x, const double &y) : x(x), y(y) {}

/*
 * two points are considered equal if the hold equal double values
 */
bool Point::operator==(const Point &p2) const
{
    return this->x == p2.x && this->y == p2.y;
}

double Point::getX() const
{
    return this->x;
}

double Point::getY() const
{
    return this->y;
}

double Point::distance(const Point *point) const
{
    return sqrt((x-point->getX())*(x-point->getX())
	      +(y-point->getY())*(y-point->getY()));
}

void Point::print(ostream &os) const
{
    os << "(" << this->x << "," << this->y << ")";
}

ostream &operator<<(ostream &os, const Point &point)
{
    point.print(os); // assuming you define print for matrix
    return os;
}