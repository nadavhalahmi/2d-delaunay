#pragma once
#include <vector>
#include "Point.hpp"

using namespace std;

class Polygon
{
private:
    vector<Point *> points;

public:
    void insert(Point* p);
    void print(ostream &os) const;
};
ostream &operator<<(ostream &os, const Polygon &polygon);