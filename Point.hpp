#pragma once
#include <iostream>

using namespace std;

class Point
{
private:
    double x, y;

public:
    Point() = default;
    Point(const double &x, const double &y);
    bool operator==(const Point &p2) const;
    double getX() const;
    double getY() const;
    double distance(const Point* point) const;
    void print(ostream &os) const;
};
ostream &operator<<(ostream &os, const Point &point);